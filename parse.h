#ifndef _PARSE_H_
#define _PARSE_H_

#include <stdio.h>
#include "container.h"

#define PARSE_INCLUDE_MAX 16
#define PARSE_SEARCH_MAX  16

struct parse_struct {
	container_t *container;
	int level;
	const char *filename;
	int include_count;
	const char *include[PARSE_INCLUDE_MAX];
	int search_count;
	const char *search[PARSE_SEARCH_MAX];
};

typedef struct parse_struct parse_t;

void parse_init(parse_t *p, container_t *c);
int parse_add_include_path(parse_t *p, const char *path);
int parse_add_search_path(parse_t *p, const char *path);
int parse_start(parse_t *p, const char *filename);
int parse_include(parse_t *p, const char *filename, int line_nr, const char *line);

int parse_cpio(parse_t *p, FILE *f);
int parse_list(parse_t *p, FILE *f);

#endif

