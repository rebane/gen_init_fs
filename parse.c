#include "parse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <dirent.h>
#include <libgen.h>
#include <sys/stat.h>
#include "container.h"

static char *parse_search_path(parse_t *p, const char *filename, const char *path, const char *name);
static void parse_setroot(parse_t *p, const char *filename);
static int parse(parse_t *p, FILE *f);

void parse_init(parse_t *p, container_t *c)
{
	p->container = c;
	p->level = 0;
	p->include_count = 0;
	p->search_count = 0;
}

int parse_add_include_path(parse_t *p, const char *path)
{
	if (p->include_count >= PARSE_INCLUDE_MAX)
		return -1;

	p->include[p->include_count++] = path;
	return 0;
}

int parse_add_search_path(parse_t *p, const char *path)
{
	if (p->search_count >= PARSE_SEARCH_MAX)
		return -1;

	p->search[p->search_count++] = path;
	return 0;
}

int parse_start(parse_t *p, const char *filename)
{
	FILE *f;
	int ret;

	if (!strcmp(filename, "-")) {
		p->filename = "STDIN";
		f = stdin;
	} else {
		p->filename = filename;
		f = fopen(filename, "r");
		if(f == NULL) {
			fprintf(stderr, "ERROR: cannot open file: '%s'\n", p->filename);
			return -1;
		}
		parse_setroot(p, filename);
	}

	ret = parse(p, f);

	if (!strcmp(filename, "-"))
		fclose(f);

	return ret;
}

int parse_include(parse_t *p, const char *filename, int line_nr, const char *line)
{
	const char *previous_filename;
	char path[PATH_MAX + 16];
	char *s;
	FILE *f;
	int ret;
	int i;

	if (p->level >= 16) {
		fprintf(stderr, "ERROR: %s: too many include levels, line: %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}


	f = fopen(filename, "r");
	if(f == NULL) {
		for (i = 0; i < p->include_count; i++) {
			snprintf(path, PATH_MAX + 16, "%s/%s", p->include[i], filename);
			path[PATH_MAX + 15] = 0;
			f = fopen(path, "r");
			if(f != NULL) {
				filename = path;
				break;
			}
		}
	}

	if(f == NULL) {
		for (i = 0; i < p->search_count; i++) {
			s = parse_search_path(p, filename, p->search[i], NULL);
			if (s != NULL) {
				snprintf(path, PATH_MAX + 16, "%s", s);
				free(s);
				path[PATH_MAX + 15] = 0;
				f = fopen(path, "r");
				if(f != NULL) {
					filename = path;
					break;
				}
			}
		}
	}

	if (f == NULL) {
		fprintf(stderr, "ERROR: %s: cannot include, line: %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}

	parse_setroot(p, filename);

	p->level++;
	previous_filename = p->filename;
	p->filename = filename;
	ret = parse(p, f);
	p->filename = previous_filename;
	p->level--;
	fclose(f);

	return ret;
}

static void parse_setroot(parse_t *p, const char *filename)
{
	char *f, *s;
	f = strdup(filename);
	asprintf(&s, "%s/root", dirname(f));
	setenv("ROOT", s, 1);
	free(s);
	free(f);
}

static char *parse_search_path(parse_t *p, const char *filename, const char *path, const char *name)
{
	struct dirent *d;
	struct stat st;
	char *newdir, *filepath;
	DIR *dir;

	if (lstat(path, &st))
		return NULL;


	if (S_ISDIR(st.st_mode)) {
		dir = opendir(path);
		if (dir) {
			while ((d = readdir(dir))) {
				if (!strcmp(d->d_name, "."))
					continue;
				if (!strcmp(d->d_name, ".."))
					continue;

				asprintf(&newdir, "%s/%s", path, d->d_name);
				filepath = parse_search_path(p, filename, newdir, d->d_name);
				if (filepath != NULL)
					return filepath;
				free(newdir);
			}
			closedir(dir);
		}
	} else {
		if ((name != NULL) && !strcmp(name, filename))
			return (char *)path;
	}

	return NULL;
}

static int parse(parse_t *p, FILE *f)
{
	char buffer[6];
	int i, l;

	for (l = 0; l < 6; l++) {
		i = fgetc(f);
		if(i == EOF)
			break;
		buffer[l] = i;
	}

	if(l <= 0)
		return -1;

	for (i = l - 1; i >= 0; i--)
		ungetc(buffer[i], f);

	if ((l == 6) && !memcmp(buffer, "070701", 6))
		return parse_cpio(p, f);
	else
		return parse_list(p, f);
}

