#include <stdio.h>
#include <unistd.h>
#include <glib.h>
#include "container.h"
#include "parse.h"
#include "writer_cpio.h"
#include "writer_jffs2.h"

int main_callback_debug(container_t *c, void *user, container_node_t *cn)
{
	if (cn->type == CONTAINER_TYPE_FILE) {
		printf("FILE:  %s, LEN: %lu\n", cn->path, cn->filesize);
	} else if (cn->type == CONTAINER_TYPE_DIR) {
		printf("DIR:   %s\n", cn->path);
	} else if (cn->type == CONTAINER_TYPE_CHAR) {
		printf("CHAR:  %s, MAJOR: %u, MINOR: %u\n", cn->path, cn->devmajor, cn->devminor);
	} else if (cn->type == CONTAINER_TYPE_BLOCK) {
		printf("BLOCK: %s, MAJOR: %u, MINOR: %u\n", cn->path, cn->devmajor, cn->devminor);
	} else if (cn->type == CONTAINER_TYPE_SLINK) {
		printf("SLINK: %s, TARGET: %s\n", cn->path, (char *)cn->data);
	} else if (cn->type == CONTAINER_TYPE_PIPE) {
		printf("PIPE:  %s\n", cn->path);
	} else if (cn->type == CONTAINER_TYPE_SOCK) {
		printf("SOCK:  %s\n", cn->path);
	}
	return 0;
}

int main(int argc, char *argv[])
{
	container_t *c;
	parse_t p;
	writer_cpio_t wc;
	writer_jffs2_t wj;
	char *filename = NULL;
	char *jffs2_dir;
	int format = 0;
	int a;

	c = container_new();
	if(c == NULL)
		return 1;

	parse_init(&p, c);

	opterr = 0;
	while ((a = getopt(argc, argv, "-hI:S:cj:")) != -1) {
		if (a == 'h') {
			fprintf(stderr, "HELP\n");
			return 0;
		} else if (a == 'I') {
			parse_add_include_path(&p, optarg);
		} else if (a == 'S') {
			parse_add_search_path(&p, optarg);
		} else if (a == 'c') {
			format = 1;
		} else if (a == 'j') {
			format = 2;
			jffs2_dir = optarg;
		} else if (a == '\1') {
			filename = optarg;
		}
	}

	if (filename == NULL)
		filename = "-";

	if (parse_start(&p, filename) < 0)
		return 1;

	if (format == 0) {
		container_iterate(c, NULL, main_callback_debug);
	} else if (format == 1) {
		writer_cpio_init(&wc, stdout);
		container_iterate(c, &wc, writer_cpio_callback);
		writer_cpio_end(&wc);
	} else if (format == 2) {
		writer_jffs2_init(&wj, jffs2_dir);
		container_iterate(c, &wj, writer_jffs2_callback);
		writer_jffs2_end(&wj);
	}

	return 0;
}

