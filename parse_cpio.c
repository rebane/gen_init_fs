#include "parse.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <sys/stat.h>
#include "container.h"

struct cpio_header_struct {
	char c_magic[6];
	char c_ino[8];
	char c_mode[8];
	char c_uid[8];
	char c_gid[8];
	char c_nlink[8];
	char c_mtime[8];
	char c_filesize[8];
	char c_devmajor[8];
	char c_devminor[8];
	char c_rdevmajor[8];
	char c_rdevminor[8];
	char c_namesize[8];
	char c_check[8];
};

static unsigned long int parse_number(char *ptr)
{
	char buffer[9];

	memcpy(buffer, ptr, 8);
	buffer[8] = 0;
	return strtoul(buffer, NULL, 16);
}

int parse_cpio(parse_t *p, FILE *f)
{
	char filename[PATH_MAX + 16];
	struct cpio_header_struct ch;
	int type, uid, gid, maj, min, namesize;
	unsigned long int mode, count, offset;
	char *name;
	void *data;

	offset = 0;
	while (!feof(f)) {
		if (fread(&ch, 110, 1, f) != 1)
			break;

		offset += 110;

		if (memcmp(ch.c_magic, "070701", 6)) {
			printf("%02X%02X%02X%02X%02X%02X\n",
				(unsigned int)ch.c_magic[0],
				(unsigned int)ch.c_magic[1],
				(unsigned int)ch.c_magic[2],
				(unsigned int)ch.c_magic[3],
				(unsigned int)ch.c_magic[4],
				(unsigned int)ch.c_magic[5]
			);
			break;
		}

		mode = parse_number(ch.c_mode);
		uid = parse_number(ch.c_uid);
		gid = parse_number(ch.c_gid);
		count = parse_number(ch.c_filesize);
		maj = parse_number(ch.c_rdevmajor);
		min = parse_number(ch.c_rdevminor);
		namesize = parse_number(ch.c_namesize);

		if (namesize > PATH_MAX)
			break;

		if (fread(&filename[1], namesize, 1, f) != 1)
			break;

		offset += namesize;

		while (offset & 3) {
			fgetc(f);
			offset++;
		}

		filename[namesize + 1] = 0;
		name = &filename[1];

		if (!strcmp(name, "TRAILER!!!"))
			break;

		if (name[0] != '/') {
			filename[0] = '/';
			name = filename;
			namesize++;
		}

		if (count) {
			data = malloc(count);
			if (data == NULL)
				break;

			if (fread(data, count, 1, f) != 1)
				break;

			offset += count;
		}

		while (offset & 3) {
			fgetc(f);
			offset++;
		}

		if (S_ISREG(mode)) {
			type = CONTAINER_TYPE_FILE;
		}else if (S_ISDIR(mode)) {
			type = CONTAINER_TYPE_DIR;
		}else if (S_ISCHR(mode)) {
			type = CONTAINER_TYPE_CHAR;
		}else if (S_ISBLK(mode)) {
			type = CONTAINER_TYPE_BLOCK;
		}else if (S_ISLNK(mode)) {
			type = CONTAINER_TYPE_SLINK;
		}else if (S_ISFIFO(mode)) {
			type = CONTAINER_TYPE_PIPE;
		}else if (S_ISSOCK(mode)) {
			type = CONTAINER_TYPE_SOCK;
		} else {
			continue;
		}
		container_add(p->container, type, name, (mode & 0xFFF), uid, gid, maj, min, data, count);
	}
	return 0;
}

