#ifndef _WRITER_JFFS2_H_
#define _WRITER_JFFS2_H_

#include <stdio.h>
#include "container.h"

struct writer_jffs2_struct {
	char *dir;
	FILE *f;
};

typedef struct writer_jffs2_struct writer_jffs2_t;

int writer_jffs2_init(writer_jffs2_t *w, char *dir);
int writer_jffs2_callback(container_t *c, void *user, container_node_t *cn);
int writer_jffs2_end(writer_jffs2_t *w);

#endif

