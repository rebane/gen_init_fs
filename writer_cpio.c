#include "writer_cpio.h"
#include "container.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>

static int cpio_trailer(writer_cpio_t *w);

int writer_cpio_init(writer_cpio_t *w, FILE *f)
{
	w->offset = 0;
	w->f = f;
	return 0;
}

int writer_cpio_end(writer_cpio_t *w)
{
	return cpio_trailer(w);
}

static void push_string(writer_cpio_t *w, const char *name)
{
	unsigned int name_len = strlen(name) + 1;

	fputs(name, w->f);
	fputc(0, w->f);
	w->offset += name_len;
}

static void push_pad(writer_cpio_t *w)
{
	while (w->offset & 3) {
		fputc(0, w->f);
		w->offset++;
	}
}

static void push_rest(writer_cpio_t *w, const char *name)
{
	unsigned int name_len = strlen(name) + 1;
	unsigned int tmp_ofs;

	fputs(name, w->f);
	fputc(0, w->f);
	w->offset += name_len;

	tmp_ofs = name_len + 110;
	while (tmp_ofs & 3) {
		fputc(0, w->f);
		w->offset++;
		tmp_ofs++;
	}
}

static void push_hdr(writer_cpio_t *w, const char *s)
{
	fputs(s, w->f);
	w->offset += 110;
}

static int cpio_trailer(writer_cpio_t *w)
{
	char s[256];
	const char name[] = "TRAILER!!!";

	sprintf(s, "%s%08X%08X%08lX%08lX%08X%08lX"
	       "%08X%08X%08X%08X%08X%08X%08X",
		"070701",		/* magic */
		0,			/* ino */
		0,			/* mode */
		(long) 0,		/* uid */
		(long) 0,		/* gid */
		1,			/* nlink */
		(long) 0,		/* mtime */
		0,			/* filesize */
		0,			/* major */
		0,			/* minor */
		0,			/* rmajor */
		0,			/* rminor */
		(unsigned)strlen(name) + 1, /* namesize */
		0);			/* chksum */

	push_hdr(w, s);
	push_rest(w, name);

	while (w->offset % 512) {
		fputc(0, w->f);
		w->offset++;
	}
	return 0;
}

static int cpio_mkslink(writer_cpio_t *w, container_node_t *cn)
{
	char s[256];

	if (cn->path[0] == '/')
		cn->path++;

	sprintf(s,"%s%08X%08X%08lX%08lX%08X%08lX"
	       "%08X%08X%08X%08X%08X%08X%08X",
		"070701",		/* magic */
		cn->ino,		/* ino */
		S_IFLNK | cn->mode,	/* mode */
		(long) cn->uid,		/* uid */
		(long) cn->gid,		/* gid */
		cn->nlink,		/* nlink */
		(long) cn->mtime,	/* mtime */
		(unsigned)cn->filesize, /* filesize */
		3,			/* major */
		1,			/* minor */
		0,			/* rmajor */
		0,			/* rminor */
		(unsigned)strlen(cn->path) + 1,/* namesize */
		0);			/* chksum */

	push_hdr(w, s);
	push_string(w, cn->path);
	push_pad(w);
	push_string(w, cn->data);
	push_pad(w);

	return 0;
}

static int cpio_mkgeneric(writer_cpio_t *w, container_node_t *cn, unsigned int mode)
{
	char s[256];

	if (cn->path[0] == '/')
		cn->path++;

	sprintf(s,"%s%08X%08X%08lX%08lX%08X%08lX"
	       "%08X%08X%08X%08X%08X%08X%08X",
		"070701",		/* magic */
		cn->ino,		/* ino */
		mode,			/* mode */
		(long) cn->uid,		/* uid */
		(long) cn->gid,		/* gid */
		2,			/* nlink */
		(long) cn->mtime,	/* mtime */
		0,			/* filesize */
		3,			/* major */
		1,			/* minor */
		0,			/* rmajor */
		0,			/* rminor */
		(unsigned)strlen(cn->path) + 1,/* namesize */
		0);			/* chksum */

	push_hdr(w, s);
	push_rest(w, cn->path);

	return 0;
}

static int cpio_mknod(writer_cpio_t *w, container_node_t *cn)
{
	unsigned int mode;
	char s[256];

	mode = cn->mode;

	if (cn->type == CONTAINER_TYPE_BLOCK)
		mode |= S_IFBLK;
	else
		mode |= S_IFCHR;

	if (cn->path[0] == '/')
		cn->path++;

	sprintf(s,"%s%08X%08X%08lX%08lX%08X%08lX"
	       "%08X%08X%08X%08X%08X%08X%08X",
		"070701",		/* magic */
		cn->ino,		/* ino */
		mode,			/* mode */
		(long) cn->uid,		/* uid */
		(long) cn->gid,		/* gid */
		1,			/* nlink */
		(long) cn->mtime,	/* mtime */
		0,			/* filesize */
		3,			/* major */
		1,			/* minor */
		cn->devmajor,		/* rmajor */
		cn->devminor,		/* rminor */
		(unsigned)strlen(cn->path) + 1,/* namesize */
		0);			/* chksum */

	push_hdr(w, s);
	push_rest(w, cn->path);
	return 0;
}

static int cpio_mkfile(writer_cpio_t *w, container_node_t *cn)
{
	char s[256];

	if (cn->path[0] == '/')
		cn->path++;

	sprintf(s,"%s%08X%08X%08lX%08lX%08X%08lX"
	       "%08lX%08X%08X%08X%08X%08X%08X",
		"070701",		/* magic */
		cn->ino,		/* ino */
		cn->mode | S_IFREG,	/* mode */
		(long) cn->uid,		/* uid */
		(long) cn->gid,		/* gid */
		2,			/* nlink */
		(long) 0,		/* mtime */
		cn->filesize,		/* filesize */
		3,			/* major */
		1,			/* minor */
		0,			/* rmajor */
		0,			/* rminor */
		(unsigned)strlen(cn->path) + 1,	/* namesize */
		0);			/* chksum */

	push_hdr(w, s);
	push_string(w, cn->path);
	push_pad(w);

	if (cn->filesize) {
		if (fwrite(cn->data, cn->filesize, 1, w->f) != 1) {
			fprintf(stderr, "writing filebuf failed\n");
			return -1;
		}
		w->offset += cn->filesize;
		push_pad(w);
	}

	return 0;
}

int writer_cpio_callback(container_t *c, void *user, container_node_t *cn)
{
	writer_cpio_t *w = (writer_cpio_t *)user;

	if (cn->type == CONTAINER_TYPE_FILE) {
		return cpio_mkfile(w, cn);
	} else if (cn->type == CONTAINER_TYPE_DIR) {
		return cpio_mkgeneric(w, cn, cn->mode | S_IFDIR);
	} else if (cn->type == CONTAINER_TYPE_CHAR) {
		return cpio_mknod(w, cn);
	} else if (cn->type == CONTAINER_TYPE_BLOCK) {
		return cpio_mknod(w, cn);
	} else if (cn->type == CONTAINER_TYPE_SLINK) {
		return cpio_mkslink(w, cn);
	} else if (cn->type == CONTAINER_TYPE_PIPE) {
		return cpio_mkgeneric(w, cn, cn->mode | S_IFIFO);
	} else if (cn->type == CONTAINER_TYPE_SOCK) {
		return cpio_mkgeneric(w, cn, cn->mode | S_IFSOCK);
	}
	return 0;
}

