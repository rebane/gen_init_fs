#ifndef _CONTAINER_H_
#define _CONTAINER_H_

enum{
	CONTAINER_TYPE_FILE,
	CONTAINER_TYPE_DIR,
	CONTAINER_TYPE_CHAR,
	CONTAINER_TYPE_BLOCK,
	CONTAINER_TYPE_SLINK,
	CONTAINER_TYPE_PIPE,
	CONTAINER_TYPE_SOCK
};

struct container_node_struct{
	char *path;
	int type;
	unsigned int ino;
	unsigned int mode;
	unsigned int uid;
	unsigned int gid;
	unsigned int nlink;
	unsigned int mtime;
	unsigned int devmajor;
	unsigned int devminor;
	unsigned long int filesize;
	void *data;
};

typedef struct container_struct container_t;
typedef struct container_node_struct container_node_t;
typedef int (*container_callback_func)(container_t *c, void *user, container_node_t *cn);

container_t *container_new();
int container_add(container_t *c, int type, const char *path, unsigned int mode, unsigned int uid, unsigned int gid, unsigned int maj, unsigned int min, void *data, unsigned long int count);
int container_iterate(container_t *c, void *user, container_callback_func callback);

#endif

