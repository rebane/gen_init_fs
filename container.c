#include "container.h"
#include <stdio.h>
#include <stdlib.h>
#include <glib.h>

struct container_struct {
	GHashTable *dirs;
	GHashTable *others;
	unsigned int ino;
};

static gint container_compare(gconstpointer a, gconstpointer b);

container_t *container_new()
{
	container_t *c;

	c = malloc(sizeof(container_t));
	if (c == NULL)
		return NULL;

	c->dirs = g_hash_table_new(g_str_hash, g_str_equal);
	if (c->dirs == NULL) {
		free(c);
		return NULL;
	}
	c->others = g_hash_table_new(g_str_hash, g_str_equal);
	if (c->others == NULL) {
		g_hash_table_unref(c->dirs);
		free(c);
		return NULL;
	}
	c->ino = 721;
	return(c);
}

int container_add(container_t *c, int type, const char *path, unsigned int mode, unsigned int uid, unsigned int gid, unsigned int maj, unsigned int min, void *data, unsigned long int count)
{
	container_node_t *cn, *cn_old;
	static GHashTable *table;

	if (type == CONTAINER_TYPE_DIR) {
		if (g_hash_table_lookup_extended(c->others, path, NULL, NULL))
			return -1;
	} else {
		if (g_hash_table_lookup_extended(c->dirs, path, NULL, NULL))
			return -1;
	}

	cn = malloc(sizeof(container_node_t));

	if(cn == NULL)
		return -1;

	cn->path = strdup(path);
	if (cn->path == NULL) {
		free(cn);
		return -1;
	}

	cn->type = type;
	cn->ino = c->ino++;
	cn->mode = mode;
	cn->uid = uid;
	cn->gid = gid;
	cn->nlink = 2;
	cn->mtime = 0;
	cn->devmajor = maj;
	cn->devminor = min;
	cn->filesize = count;
	cn->data = data;

	if (type == CONTAINER_TYPE_DIR)
		table = c->dirs;
	else
		table = c->others;

	if (g_hash_table_lookup_extended(table, cn->path, NULL, (gpointer *)&cn_old)) {
		g_hash_table_remove(table, cn->path);
		g_hash_table_insert(table, cn->path, cn);

		if (cn_old->filesize)
			free(cn_old->data);

		free(cn_old->path);
		free(cn_old);
	} else {
		g_hash_table_insert(table, cn->path, cn);
	}

	return 0;
}

int container_iterate(container_t *c, void *user, container_callback_func callback)
{
	container_node_t *cn;
	GList *list = NULL, *l;
	int ret = 0;

	list = g_hash_table_get_values(c->dirs);
	if (list != NULL) {
		list = g_list_sort(list, container_compare);
		for (l = list; l != NULL; l = l->next) {
			cn = (container_node_t *)(l->data);
			ret = callback(c, user, cn);
			if (ret) {
				g_list_free(list);
				return ret;
			}
		}
		g_list_free(list);
	}

	list = g_hash_table_get_values(c->others);
	if (list != NULL) {
		list = g_list_sort(list, container_compare);
		for (l = list; l != NULL; l = l->next) {
			cn = (container_node_t *)(l->data);
			ret = callback(c, user, cn);
			if (ret) {
				g_list_free(list);
				return ret;
			}
		}
		g_list_free(list);
	}

	return 0;
}

static gint container_compare(gconstpointer a, gconstpointer b)
{
	container_node_t *cn_a, *cn_b;

	cn_a = (container_node_t *)a;
	cn_b = (container_node_t *)b;

	return strcmp(cn_a->path, cn_b->path);
}

