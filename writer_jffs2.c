#include "writer_jffs2.h"
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <fcntl.h>
#include <errno.h>
#include <ctype.h>
#include <limits.h>

int writer_jffs2_init(writer_jffs2_t *w, char *dir)
{
	char filename[PATH_MAX + 1];

	w->dir = dir;
	mkdir(dir, 0755);
	snprintf(filename, PATH_MAX, "%s/root", w->dir);
	filename[PATH_MAX] = 0;
	mkdir(filename, 0755);
	snprintf(filename, PATH_MAX, "%s/list.txt", w->dir);
	filename[PATH_MAX] = 0;

	w->f = fopen(filename, "w");
	if (w->f == NULL)
		return -1;

	return 0;
}

int writer_jffs2_end(writer_jffs2_t *w)
{
	fclose(w->f);
	return 0;
}

int writer_jffs2_callback(container_t *c, void *user, container_node_t *cn)
{
	writer_jffs2_t *w = (writer_jffs2_t *)user;
	char filename[PATH_MAX + 1];
	int fd;

	if (cn->type == CONTAINER_TYPE_FILE) {
		fprintf(w->f, "%s f %o %u %u - - - - -\n", cn->path, cn->mode, cn->uid, cn->gid);
		snprintf(filename, PATH_MAX, "%s/root/%s", w->dir, cn->path);
		filename[PATH_MAX] = 0;
		fd = open(filename, O_WRONLY | O_CREAT | O_TRUNC, cn->mode);
		write(fd, cn->data, cn->filesize);
		close(fd);
	} else if (cn->type == CONTAINER_TYPE_DIR) {
		fprintf(w->f, "%s d %o %u %u - - - - -\n", cn->path, cn->mode, cn->uid, cn->gid);
		snprintf(filename, PATH_MAX, "%s/root/%s", w->dir, cn->path);
		filename[PATH_MAX] = 0;
		mkdir(filename, 0755);
	} else if (cn->type == CONTAINER_TYPE_CHAR) {
		fprintf(w->f, "%s c %o %u %u %u %u - - -\n", cn->path, cn->mode, cn->uid, cn->gid, cn->devmajor, cn->devminor);
	} else if (cn->type == CONTAINER_TYPE_BLOCK) {
		fprintf(w->f, "%s b %o %u %u %u %u - - -\n", cn->path, cn->mode, cn->uid, cn->gid, cn->devmajor, cn->devminor);
	} else if (cn->type == CONTAINER_TYPE_SLINK) {
//		fprintf(w->f, "%s l %o %u %u - - - - -\n", cn->path, cn->mode, cn->uid, cn->gid);
		snprintf(filename, PATH_MAX, "%s/root/%s", w->dir, cn->path);
		filename[PATH_MAX] = 0;
		symlink(cn->data, filename);
	} else if (cn->type == CONTAINER_TYPE_PIPE) {
		fprintf(w->f, "%s p %o %u %u - - - - -\n", cn->path, cn->mode, cn->uid, cn->gid);
	} else if (cn->type == CONTAINER_TYPE_SOCK) {
//		fprintf(w->f, "%s s %o %u %u - - - - -\n", cn->path, cn->mode, cn->uid, cn->gid);
	}
	return 0;
}

