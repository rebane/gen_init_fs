#ifndef _WRITER_CPIO_H_
#define _WRITER_CPIO_H_

#include <stdio.h>
#include "container.h"

struct writer_cpio_struct {
	unsigned long int offset;
	FILE *f;
};

typedef struct writer_cpio_struct writer_cpio_t;

int writer_cpio_init(writer_cpio_t *w, FILE *f);
int writer_cpio_callback(container_t *c, void *user, container_node_t *cn);
int writer_cpio_end(writer_cpio_t *w);

#endif

