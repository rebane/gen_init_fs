#include "parse.h"
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <limits.h>
#include "container.h"

#define xstr(s) #s
#define str(s) xstr(s)
#define LINE_SIZE (2 * PATH_MAX + 50)

static void *get_file(const char *filename, unsigned long int *l)
{
	void *data = NULL;
	struct stat s;
	int fd;

	fd = open(filename, O_RDONLY);
	if (fd < 0)
		return NULL;

	if (fstat(fd, &s) < 0)
		goto fail;

	data = malloc(s.st_size);
	if (data == NULL)
		goto fail;

	if (read(fd, data, s.st_size) != s.st_size)
		goto fail;

	close(fd);
	*l = s.st_size;
	return(data);
fail:
	if (data)
		free(data);

	close(fd);
	return(NULL);
}

static char *replace_env(char *new_location)
{
	char expanded[PATH_MAX + 1];
	char *start, *end, *var;

	while ((start = strstr(new_location, "${")) &&
	       (end = strchr(start + 2, '}'))) {
		*start = *end = 0;
		var = getenv(start + 2);
		snprintf(expanded, sizeof expanded, "%s%s%s",
			 new_location, var ? var : "", end + 1);
		strcpy(new_location, expanded);
	}

	return new_location;
}

static int mkgeneric_line(parse_t *p, int line_nr, const char *line, int type, const char *gen)
{
	char name[PATH_MAX + 1];
	unsigned int mode;
	int uid;
	int gid;

	if (4 != sscanf(line, "%" str(PATH_MAX) "s %o %d %d", name, &mode, &uid, &gid)) {
		fprintf(stderr, "ERROR: %s: unrecognized %s format, line: %d '%s'\n",
			p->filename, gen, line_nr, line);
		return -1;
	}

	return container_add(p->container, type, name, mode, uid, gid, 0, 0, NULL, 0);
}

static int mkfile_line(parse_t *p, int line_nr, const char *line)
{
	char location[PATH_MAX + 1];
	char name[PATH_MAX + 1];
	unsigned long int l;
	unsigned int mode;
	void *data = NULL;
	int end = 0;
	int uid;
	int gid;

	if (5 > sscanf(line, "%" str(PATH_MAX) "s %" str(PATH_MAX)
				"s %o %d %d %n",
				name, location, &mode, &uid, &gid, &end)) {
		fprintf(stderr, "ERROR: %s: unrecognized format, line %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}

	replace_env(location);
	data = get_file(location, &l);
	if (data == NULL) {
		fprintf(stderr, "ERROR: %s: unable to open file, line %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}

	return container_add(p->container, CONTAINER_TYPE_FILE, name, mode, uid, gid, 0, 0, data, l);
}

static int mknod_line(parse_t *p, int line_nr, const char *line)
{
	char name[PATH_MAX + 1];
	unsigned int mode;
	unsigned int maj;
	unsigned int min;
	char dev_type;
	int uid;
	int gid;

	if (7 != sscanf(line, "%" str(PATH_MAX) "s %o %d %d %c %u %u",
			 name, &mode, &uid, &gid, &dev_type, &maj, &min)) {
		fprintf(stderr, "ERROR: %s: unrecognized nod format, line %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}

	if (dev_type == 'b')
		return container_add(p->container, CONTAINER_TYPE_BLOCK, name, mode, uid, gid, maj, min, NULL, 0);
	else
		return container_add(p->container, CONTAINER_TYPE_CHAR, name, mode, uid, gid, maj, min, NULL, 0);
}

static int mkslink_line(parse_t *p, int line_nr, const char *line)
{
	char target[PATH_MAX + 1];
	char name[PATH_MAX + 1];
	unsigned int mode;
	char *dtarget;
	int uid;
	int gid;
	int l;

	if (5 != sscanf(line, "%" str(PATH_MAX) "s %" str(PATH_MAX) "s %o %d %d", name, target, &mode, &uid, &gid)) {
		fprintf(stderr, "ERROR: %s: unrecognized slink format, line: %d: '%s'\n", p->filename, line_nr, line);
		return -1;
	}

	l = strlen(target) + 1;
	dtarget = malloc(l);
	if (dtarget == NULL)
		return -1;

	memcpy(dtarget, target, l);
	return container_add(p->container, CONTAINER_TYPE_SLINK, name, mode, uid, gid, 0, 0, dtarget, l);
}

int parse_list(parse_t *p, FILE *f){
	char include[LINE_SIZE];
	char line[LINE_SIZE];
	char *args, *type;
	int ec = 0, rc;
	int line_nr = 0;
	size_t slen;

	while (fgets(line, LINE_SIZE, f)) {
		slen = strlen(line);
		line_nr++;

		if (!strncmp(line, "#include", 8)) {
			line[strlen(line) - 1] = 0;
			args = line + 8;
			while (*args) {
				if (*args == '\"')
					break;
				args++;
			}
			if (*args != '\"') {
				fprintf(stderr,
					"ERROR: %s: incorrect format, line %d: '%s'\n",
					p->filename, line_nr, line);
				ec = -1;
				break;
			}
			args++;
			for (type = args + (strlen(args)); type > args; type--) {
				if (*type == '\"')
					break;
			}
			if (type <= args) {
				fprintf(stderr,
					"ERROR: %s: incorrect format, line %d: '%s'\n",
					p->filename, line_nr, line);
				ec = -1;
				break;
			}
			memcpy(include, args, type - args);
			include[type - args] = 0;
			rc = parse_include(p, include, line_nr, line);
			if (rc != 0) {
				ec = rc;
				break;
			}
			continue;
		}

		/* comment - skip to next line */
		if (line[0] == '#')
			continue;

		/* comment - skip to next line */
		if ((line[0] == '/') && (line[1] == '/'))
			continue;

		if (! (type = strtok(line, " \t"))) {
			fprintf(stderr,
				"ERROR: %s: incorrect format, line %d: '%s'\n",
				p->filename, line_nr, line);
			ec = -1;
			break;
		}

		/* a blank line */
		if (type[0] == '\n')
			continue;

		/* must be an empty line */
		if (slen == strlen(type))
			continue;

		if (! (args = strtok(NULL, "\n"))) {
			fprintf(stderr,
				"ERROR: %s: incorrect format, newline required, line %d: '%s'\n",
				p->filename, line_nr, line);
			ec = -1;
		}

		if (!strcmp(line, "file")) {
			rc = mkfile_line(p, line_nr, args);
		} else if (!strcmp(line, "nod")) {
			rc = mknod_line(p, line_nr, args);
		} else if (!strcmp(line, "dir")) {
			rc = mkgeneric_line(p, line_nr, args, CONTAINER_TYPE_DIR, line);
		} else if (!strcmp(line, "slink")) {
			rc = mkslink_line(p, line_nr, args);
		} else if (!strcmp(line, "pipe")) {
			rc = mkgeneric_line(p, line_nr, args, CONTAINER_TYPE_PIPE, line);
		} else if (!strcmp(line, "sock")) {
			rc = mkgeneric_line(p, line_nr, args, CONTAINER_TYPE_SOCK, line);
		} else {
			fprintf(stderr, "ERROR: %s: unknown file type, line %d: '%s'\n",
				p->filename, line_nr, line);
			continue;
		}
		if (rc != 0) {
			ec = rc;
			break;
		}
	}

	return ec;
}

